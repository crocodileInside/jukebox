from flask import Flask, send_file, make_response, request, current_app
import time
from mpd import MPDClient
from datetime import timedelta
from functools import update_wrapper

app = Flask(__name__)
client = MPDClient()               # create client object
client.timeout = 10                # network timeout in seconds (floats allowed), default: None
client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None




def crossdomain(origin=None, methods=None, headers=None, max_age=21600, attach_to_all=True, automatic_options=True):
  if methods is not None:
    methods = ', '.join(sorted(x.upper() for x in methods))
  if headers is not None and not isinstance(headers, basestring):
    headers = ', '.join(x.upper() for x in headers)
  if not isinstance(origin, basestring):
    origin = ', '.join(origin)
  if isinstance(max_age, timedelta):
    max_age = max_age.total_seconds()

  def get_methods():
    if methods is not None:
      return methods

    options_resp = current_app.make_default_options_response()
    return options_resp.headers['allow']

  def decorator(f):
    def wrapped_function(*args, **kwargs):
      if automatic_options and request.method == 'OPTIONS':
        resp = current_app.make_default_options_response()
      else:
        resp = make_response(f(*args, **kwargs))
      if not attach_to_all and request.method != 'OPTIONS':
        return resp

      h = resp.headers
      h['Access-Control-Allow-Origin'] = origin
      h['Access-Control-Allow-Methods'] = get_methods()
      h['Access-Control-Max-Age'] = str(max_age)
      h['Access-Control-Allow-Credentials'] = 'true'
      h['Access-Control-Allow-Headers'] = "Origin, X-Requested-With, Content-Type, Accept, Authorization"
      if headers is not None:
        h['Access-Control-Allow-Headers'] = headers
      return resp

    f.provide_automatic_options = False
    return update_wrapper(wrapped_function, f)
  return decorator

def options (self):
  return {'Allow' : 'PUT,GET,OPTIONS' }, 200, { 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods' : 'PUT,GET' }

  
@app.route("/")
def hello():
	return "Hello World!"
	
@app.route("/capture")
def getcapture():
	return "bla"

@app.route("/status")
def getstatus():
  return str(client.status())
  
@app.route("/currentsong")
def getcurrentsong():
  return str(client.currentsong())

@app.route("/songs/all")
@crossdomain(origin='*', methods=['GET', 'OPTIONS'])
def listsongs():
  return str(client.listallinfo())
  

if __name__ == "__main__":
  client.connect("localhost", 6600)  # connect to localhost:6600  
  app.run(host='0.0.0.0', port=8081, debug=True)
