from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty, ListProperty
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.listview import ListView, ListItemButton, CompositeListItem, ListItemLabel
from kivy.core.window import Window
from kivy.clock import Clock


from mpd import MPDClient

client = MPDClient()               # create client object
client.timeout = 10                # network timeout in seconds (floats allowed), default: None
client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
client.iterate = True



class JukeboxRoot(Widget):
  
  slider_songpos = ObjectProperty()
  playlist_view = ObjectProperty()
  
  def __init__(self):
    
    client.connect("localhost", 6600)
    self.current_playlist = []
    self.current_song = {}
    self.current_time = 0
    self.sliderchange_auto = False
    
    self.playlist_la = ListAdapter(data=self.current_playlist, 
      args_converter=self.playlist_formatter,
      cls=CompositeListItem,         
      selection_mode='single',
      allow_empty_selection=True)
    self.playlist_la.bind(on_selection_change=self.playlist_selection_changed)
    
    self.update_playlist()
    
    super(JukeboxRoot, self).__init__()
    
  def playlist_selection_changed(self, playlist_listadapter, *args):
    if(len(playlist_listadapter.selection) == 1):
      songid = playlist_listadapter.selection[0].id.split('_')[2]
      print songid
      client.playid(songid)
      print playlist_listadapter.selection[0].id #.get_data_item(0)
  
  def playlist_formatter(self, row_index, playlist_data):
    colsize = int((Window.size[0] * 0.3))
    return {'text': playlist_data['id'],
      'size_hint_y': None,
      'halign': 'left',
      'height': 30,
      'background_color': [0.4, 0.7, 0.1, 0.6],
      'deselected_color': [.1, .6, .33, 1],
      'cls_dicts': [
        {'cls': ListItemLabel, 'kwargs': {'text': playlist_data['title'], 'halign': 'left', 'text_size': (colsize, None) } },
        {'cls': ListItemLabel, 'kwargs': {'text': playlist_data['artist'], 'halign': 'left', 'text_size': (colsize, None) } },
        {'cls': ListItemLabel, 'kwargs': {'text': playlist_data['album'], 'halign': 'left', 'text_size': (colsize, None) } },
        {'cls': ListItemButton, 'kwargs': {'text': 'Play', 'size_hint_x': 0.2, 'id':'btn_play_'+playlist_data['id'] } }
      ]
    }  
  
  def update_playlist(self):
    del self.current_playlist[:]
    
    for song in client.playlistinfo():
      self.current_playlist.append({'id':song['id'], 'pos':song['pos'], 'title':song['title'], 'artist':song['artist'], 'album':song['album'] if 'album' in song else '', 'time':int(song['time'])})
    #print self.current_playlist
    
    self.playlist_la.data = self.current_playlist
    if(hasattr(self.playlist_view, '_reset_spopulate')):
      self.playlist_view._reset_spopulate()  # !!! The key for correct updating of the list !!!
    #print dir(self.playlist_view)
  
  def songpos_change(self):
    if(self.sliderchange_auto == False):
      if(client.status()['state'] == 'play'):
        timepos = (float(self.slider_songpos.value) / 100) * float(self.current_song['time'])
        client.seekcur(int(timepos))
        print 'seek: ' + str(timepos)
  
  def update(self, args):
    clientstate = client.status()['state']
    if(clientstate == 'play' or clientstate == 'pause'):
      song = client.currentsong()
      self.current_song = {'id':song['id'], 'pos':song['pos'], 'title':song['title'], 'artist':song['artist'], 'album':song['album'] if 'album' in song else '', 'time':int(song['time'])}
      self.current_time = int(float(client.status()['elapsed']))
      
      sliderpos = (float(self.current_time)/float(self.current_song['time'])) * 100
      self.sliderchange_auto = True
      self.slider_songpos.value = sliderpos
      self.sliderchange_auto = False
      
    self.update_playlist()
    
  
  def song_next(self):
    client.next()
  
  def song_prev(self):
    client.previous()
    
  def song_play(self):
    clientstate = client.status()['state']
    if(clientstate == 'stop'):
      client.play(0)
    elif(clientstate == 'play'):
      client.pause(1)
    elif(clientstate == 'pause'):
      client.pause(0)
    
  pass


class JukeboxApp(App):
  def build(self):
    jukebox = JukeboxRoot()
    Clock.schedule_interval(jukebox.update, 1.0)
    return jukebox


if __name__ == '__main__':
  JukeboxApp().run()